<?php

namespace app\models;

use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $lft
 * @property int $rgt
 * @property int $lvl
 * @property string $title
 */
class Category extends ActiveRecord
{
    public $parentId;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['lft', 'rgt', 'lvl', 'parentId'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['lft', 'rgt', 'lvl'], 'safe'],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function deleteWithChildren()
    {
        $success = true;
        $diff = ($this->childrenCount() + 1) * 2;
        $lft = $this->lft;
        $children = $this->children();
        foreach ($children as $child) $child->delete();
        if ($this->lvl != 0) $this->delete();

        /** @var self[] $changedElements */
        $changedElements = self::find()
            ->andWhere([
                'or',
                ['>', 'lft', $lft],
                ['>', 'rgt', $lft],
            ])
            ->all();
        foreach ($changedElements as $element){
            if ($element->lft > $lft) $element->lft -= $diff;
            $element->rgt -= $diff;
            $success = $success & $element->save();
        }
        if ($this->lvl === 0) {
            $this->rgt = $lft + 1;
            $success = $this->save();
        }

        return $success;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lft' => Yii::t('app', 'Lft'),
            'rgt' => Yii::t('app', 'Rgt'),
            'lvl' => Yii::t('app', 'Level'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return bool
     */
    public function makeRoot(){
        $this->lft = 1;
        $this->rgt = 2;
        $this->lvl = 0;

        return $this->save();
    }

    /**
     * @param self $parent
     * @return bool
     */
    public function prependTo($parent){
        /** @var self[] $allElements */
        $allElements = self::find()->all();
        $this->lft = $parent->lft + 1;
        $this->rgt = $parent->lft + 2;
        $this->lvl = $parent->lvl + 1;
        $success = true;
        foreach ($allElements as $element){
            if ($element->lft > $parent->lft) $element->lft += 2;
            if ($element->rgt > $parent->lft) {
                $element->rgt += 2;
                $success = $success & $element->save();
            }
        }

        return $this->save() & $success;
    }
    /**
     * @param self $parent
     * @return bool
     */
    public function appendTo($parent){
        /** @var self[] $allElements */
        $allElements = self::find()->all();
        $this->lft = $parent->rgt;
        $this->rgt = $parent->rgt + 1;
        $this->lvl = $parent->lvl + 1;
        $success = true;
        foreach ($allElements as $element){
            if ($element->lft > $parent->rgt) $element->lft += 2;
            if ($element->rgt >= $parent->rgt) {
                $element->rgt += 2;
                $success = $success & $element->save();
            }
        }

        return $this->save() & $success;
    }

    /**
     * @param Category $node
     * @return bool
     */
    public function insertBefore($node){
        /** @var self[] $nodeChildren */
        /** @var self[] $thisChildren */
        $diffNode = ($node->childrenCount() + 1) * 2;
        $diffThis = ($this->childrenCount() + 1) * 2;
        $nodeChildren = $node->children();
        $thisChildren = $this->children();

        $success = true;
        $node->lft += $diffThis;
        $node->rgt += $diffThis;
        $this->lft -= $diffNode;
        $this->rgt -= $diffNode;
        $success = $success & $node->save() & $this->save();
        foreach ($nodeChildren as $element){
            $element->rgt += $diffThis;
            $element->lft += $diffThis;
            $success = $success & $element->save();
        }
        foreach ($thisChildren as $element){
            $element->rgt -= $diffNode;
            $element->lft -= $diffNode;
            $success = $success & $element->save();
        }

        return $success;
    }
    /**
     * * @param Category $node
     * @return bool
     */
    public function insertAfter($node){
        /** @var self[] $nodeChildren */
        /** @var self[] $thisChildren */
        $diffNode = ($node->childrenCount() + 1) * 2;
        $diffThis = ($this->childrenCount() + 1) * 2;
        $nodeChildren = $node->children();
        $thisChildren = $this->children();

        $success = true;
        $node->lft -= $diffThis;
        $node->rgt -= $diffThis;
        $this->lft += $diffNode;
        $this->rgt += $diffNode;
        $success = $success & $node->save() & $this->save();
        foreach ($nodeChildren as $element){
            $element->rgt -= $diffThis;
            $element->lft -= $diffThis;
            $success = $success & $element->save();
        }
        foreach ($thisChildren as $element){
            $element->rgt += $diffNode;
            $element->lft += $diffNode;
            $success = $success & $element->save();
        }

        return $success;
    }
    /**
     * @return self[]
     */
    public function children(){
        $children = self::find()
            ->andWhere(['>', 'lft', $this->lft])
            ->andWhere(['<', 'rgt', $this->rgt])
            ->all();

        return $children;
    }
    /**
     * @return int
     */
    public function childrenCount(){
        $childrenCount = self::find()
            ->andWhere(['>', 'lft', $this->lft])
            ->andWhere(['<', 'rgt', $this->rgt])
            ->count();

        return $childrenCount;
    }
    /**
     * @return self[]
     */
    public function leaves(){
        $children = self::find()
            ->andWhere(['>', 'lft', $this->lft])
            ->andWhere(['<', 'rgt', $this->rgt])
            ->andWhere(['lvl' => $this->lvl + 1])
            ->all();

        return $children;
    }
    /**
     * @return self[]
     */
    public function parent(){
        $parent = self::find()
            ->andWhere(['<', 'lft', $this->rgt])
            ->andWhere(['>', 'rgt', $this->rgt])
            ->andWhere(['lvl' => $this->lvl - 1])
            ->orderBy(['rgt' => SORT_ASC])
            ->one();

        return $parent;
    }
}
